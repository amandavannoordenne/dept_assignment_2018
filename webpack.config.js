const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const autoprefixer = require('autoprefixer');
const __DEV__ = process.env.NODE_ENV !== 'production';

module.exports = {
  entry: ['./js/app.js', './scss/style.scss'],
  output: {
    filename: 'dist/final.js'
  },
  // Automatically compile when files change.
  watch: true,
  // Automatically reload the page when compilation is done.
  devServer: {
    inline: true
  },

  module: {

    rules: [
      {
        test: /\.(scss)$/,
        loader: ExtractTextPlugin.extract(
          {use: [
          `css-loader?url=false&sourceMap=${__DEV__}`,
          {
            loader: 'postcss-loader',
            options: { sourceMap: __DEV__ ? 'inline' : false, plugins: [autoprefixer] },
          },
          { loader: 'sass-loader', options: { sourceMap: __DEV__, importLoader: true } },
        ]}
        ),
      },
    ]
  },
  plugins: [
    new ExtractTextPlugin({ // define where to save the file
      filename: 'dist/style.css',
      allChunks: true,
    }),
    new UglifyJSPlugin(),
  ],
};
