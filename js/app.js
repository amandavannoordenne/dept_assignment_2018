//import 'core-js/fn/array/from';

import { TimelineMax } from 'gsap/all';
import ScrollMagic from 'scrollmagic';
import Flickity from 'flickity';

(function () {

  let flkty = '';

  document.addEventListener('DOMContentLoaded', () => {
    let scrollIndicatorButton = document.querySelector('.js-scroll-indicator');
    let mainMenuToggleButton = document.querySelector('.js-menu-toggle');

    scrollIndicatorButton.addEventListener('click', scrollToAbout);

    mainMenuToggleButton.addEventListener('click', (e) => {
      if (window.innerWidth < 1000) {
        toggleMainMenu(e);
      }
    });

    addCarousel();
    introAnimations();
    sectionAboutAnimations();
    sectionInstagramAnimations();

    window.addEventListener('resize', () => {
      //it should be working but isn't - needs more debugging/research...
      flkty.resize();
    });

  });

  function addCarousel () {
    let carousel = document.querySelector('.carousel');

    //create hero carousel
    flkty = new Flickity(carousel, {
        pageDots: false,
        prevNextButtons: true,
        wrapAround: true,
        autoPlay: 3500,
        resize: true,
        arrowShape: 'M10.8,50c0-3.7,5.6-8.2,5.6-8.2L61.2,2.2c3.7-2.9,9.6-2.9,13.3,0c3.6,2.9,3.6,7.6,0,10.5L37.1,50l37.4,37.3c3.6,2.9,3.6,7.6,0,10.5c-3.6,2.9-9.6,2.9-13.3,0L16.5,58.3C16.5,58.2,10.8,53.8,10.8,50z',
      },
    );
  }

  function scrollToAbout () {
    let aboutSection = document.querySelector('.js-about');
    const scrollToOffset = (window.innerWidth < 1000) ? 60 : 80; //fixed positioned header height

    TweenMax.to(window, .5, {
      scrollTo: { y: aboutSection, offsetY: scrollToOffset },
      ease: Power3.easeInOut,
    });
  }

  function toggleMainMenu (e) {
    if (e.currentTarget.classList.contains('active')) {
      closeMainMenu(e);
    } else {
      openMainMenu(e);
    }
  }

  function openMainMenu (e) {
    e.currentTarget.classList.add('active');

    let mainMenu = document.querySelector('.main-menu');
    let mainMenuBg = mainMenu.querySelector('.main-menu-bg');
    let menuLinks = document.querySelectorAll('.menu-link');

    const tl = new TimelineMax();

    tl.set(mainMenu, {
      visibility: 'visible',
    })
      .set(mainMenu, {
        visibility: 'visible',
      })
      .to(mainMenu, .25, {
        opacity: 1,
      })
      .from(mainMenu, .5, {
        height: 0,
        ease: Power3.easeInOut,
      }, 0)
      .to(mainMenuBg, .6, {
        height: '100%',
        ease: Power3.easeInOut,
      }, '-=.35')
      .staggerFromTo(menuLinks, .6, {
        opacity: 0,
        x: 20,
      }, {
        opacity: 1,
        x: 0,
        ease: Back.easeOut.config(1.7),
      }, 0.1, '-=.25');
  }

  function closeMainMenu (e) {
    e.currentTarget.classList.remove('active');

    let mainMenu = document.querySelector('.main-menu');
    let mainMenuBg = mainMenu.querySelector('.main-menu-bg');
    let menuLinks = document.querySelectorAll('.menu-link');

    const tl = new TimelineMax();

    tl.staggerFromTo(menuLinks, .6, {
      opacity: 1,
      x: 0,
    }, {
      opacity: 0,
      x: 20,
      ease: Back.easeOut.config(1.7),
    }, -0.1)
      .to(mainMenu, .75, {
        height: 0,
        ease: Power3.easeInOut,
      }, 0)
      .set(mainMenu, {
        clearProps: 'all',
      })
      .set(mainMenuBg, {
        clearProps: 'all',
      })
      .set(menuLinks, {
        clearProps: 'all',
      });
  }

  function introAnimations () {

    let sectionHero = document.querySelector('.hero');
    let header = document.querySelector('header');
    let menuLinks = document.querySelectorAll('.menu-link');
    let carouselTitles = document.querySelectorAll('.carousel-item-title');
    let carouselButtons = document.querySelectorAll('.carousel-item-button');
    let carouselNav = document.querySelectorAll('.flickity-button');
    let scrollIndicator = document.querySelector('.scroll-indicator');

    const tl = new TimelineMax({ onComplete: scrollIndicatorIdleAnimation });

    tl.to(header, .3, {
      top: 0,
      ease: Power3.easeInOut,
    }, '+=0.5')
      .staggerFromTo(menuLinks, .6, {
        opacity: 0,
        y: -10,
      }, {
        opacity: 1,
        y: 0,
        ease: Back.easeOut.config(1.7),
      }, 0.1, '-=.1')
      .fromTo(carouselTitles, .6, {
        opacity: 0,
        y: -10,
      }, {
        opacity: 1,
        y: 0,
        ease: Back.easeOut.config(1.7),
      }, '-=.5')
      .fromTo(carouselButtons, .6, {
        opacity: 0,
        y: -10,
      }, {
        opacity: 1,
        y: 0,
        ease: Back.easeOut.config(1.7),
      }, '-=.4')
      .fromTo(carouselNav, .6, {
        opacity: 0,
        y: -10,
      }, {
        opacity: 1,
        y: 0,
        ease: Back.easeOut.config(1.7),
      }, '-=.4')
      .fromTo(scrollIndicator, .6, {
        opacity: 0,
        y: -10,
      }, {
        opacity: 1,
        y: 0,
        ease: Back.easeOut.config(1.7),
      }, '-=.4')

      .to(sectionHero, 2, {
        opacity: 1,
      }, 0);
  }

  function scrollIndicatorIdleAnimation () {
    let scrollIndicatorIcon = document.querySelectorAll('.scroll-indicator-icon .arrow');

    const tl = new TimelineMax({ repeat: -1, repeatDelay: 3 });

    tl.to(scrollIndicatorIcon, .5, {
      y: 10,
      ease: Power3.easeOut,
    }, '+=1.5')
      .to(scrollIndicatorIcon, .5, {
        y: 0,
        ease: Power3.easeIn,
      }, '+=.1')
      .set(scrollIndicator, {
        clearProps: 'transform',
      });
  }

  function sectionAboutAnimations () {
    let sectionAbout = document.querySelectorAll('.js-about');

    let controller = new ScrollMagic.Controller();

    new ScrollMagic.Scene({
      triggerHook: 0.75, //trigger if element is 75% from top of page
      triggerElement: sectionAbout, // starting scene, when reaching this element
    })
      .setClassToggle(sectionAbout, 'loaded')
      .addTo(controller);
  }

  function sectionInstagramAnimations () {
    let sectionInstragram = document.querySelectorAll('.js-instagram');

    let controller = new ScrollMagic.Controller();

    new ScrollMagic.Scene({
      triggerHook: 0.75, //trigger if element is 75% from top of page
      triggerElement: sectionInstragram, // starting scene, when reaching this element
    })
      .setClassToggle(sectionInstragram, 'loaded')
      .addTo(controller);
  }

})();
